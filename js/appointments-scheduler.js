(function($) {

    function calculate_width(n, start, end, columns, columnWidth) {

        var stop = columns.length;

        for (var i = n + 1; i < columns.length; i++) {
            var flag = false;

            for (var j = 0; j < columns[i].length; j++) {
                if (columns[i][j].start > end || columns[i][j].end < start) {
                    continue;
                } else {
                    flag = true;
                    break;
                }
            }

            if (flag == true) {
                stop = i;
                break;
            }
        }

        return columnWidth * (stop - n);
    }

    function init() {

        var columns = []
        columns.push([])

        appointments.sort(function(a, b) {
            return parseFloat(a.start) - parseFloat(b.start);
        });

        $('#all_items').text(JSON.stringify(appointments));

        columns[0].push(appointments[0])

        for (var i = 1; i < appointments.length; i++) {
            var flag1 = false;
            for (var j = 0; j < columns.length; j++) {
                var flag = false;
                for (var k = 0; k < columns[j].length; k++) {
                    if (columns[j][k].end > appointments[i].start) {
                        flag = true;
                        break;
                    }
                }
                if (flag == false) {
                    columns[j].push(appointments[i])
                    flag1 = true;
                    break;
                }
            }
            if (flag1 == false) {
                columns.push([])
                columns[columns.length - 1].push(appointments[i])
            }
        }

        var columnWidth = 600 / columns.length;

        for (var i = 0; i < columns.length; i++) {
            for (var j = 0; j < columns[i].length; j++) {
                var width = calculate_width(i, columns[i][j].start, columns[i][j].end, columns, columnWidth)
                var left = columnWidth * i;
                var top = 2 * columns[i][j].start;
                var height = 2 * (columns[i][j].end - columns[i][j].start);
                var cal_item = '<div class="card" style="left:' + left + 'px;top:' + top + 'px;height:' + height + 'px;width:' + width + 'px"><span>Meeting #' + columns[i][j].id + '<br>Starts at ' + columns[i][j].start + ', ends at ' + columns[i][j].end + '</span></div>';
                $('#calendar').append(cal_item);
            }
        }
    }


    $('#new_appointment').submit(function() {
        appointments.push({
            id: parseInt($("#id").val()),
            start: parseInt($("#start").val()),
            end: parseInt($("#stop").val()),
        })
        $("#calendar").html('');
        init();
        return false;
        
    })

    init();

})(jQuery)
